import express from 'express'

import { Users } from './db'

let r = express.Router()

function user_data(session) {
    if (session.user == undefined) {
        session.user = {
            loggedIn: false
        }
    }
    return session.user;
}

function page(path, page) {
    r.get(path, (req, res) => {
        res.render(page, { user: user_data(req.session) });
    });
}

r.get('/user/:id', (req, res) => {
    res.render('index', {
        loggedIn: true,
        user: {
            id: req.params.id
        }
    });
});

r.get('/login', (req, res) => {
    let auth = Users.authenticate('Admin', 'admin', (success, user) => {
        if (success) {
            req.session.user = { loggedIn: true, id: user._id, name: user.username }
            res.redirect('/');
        } else {
            res.redirect('/?failure')
        }
    });

});

r.get('/logout', (req, res) => {
    req.session.destroy()
    res.redirect('/');
});

page('/', 'index')
page('/register/', 'register')

export default r
