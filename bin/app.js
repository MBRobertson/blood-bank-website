import express from 'express'
import session from 'express-session'
import sass from 'node-sass-middleware'

import router from './routes'
import { Config } from './config'

let app = express();

app.set('view engine', 'pug');
app.set('views', __dirname + '/views')

app.use(session({secret: Config.secret, resave: false, saveUninitialized: false, cookie: { maxAge: 60000 }}));

app.use(router)

app.use(sass({ src: __dirname + '/client/sass', dest: __dirname + '/client/css', force: true, outputStyle: 'compressed', prefix: '/css'}));
app.use(express.static(__dirname + '/client'));

let port = 3000;
app.listen(port, () => {
    console.log('Listening on port ' + port);
});
