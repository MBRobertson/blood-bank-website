import Datastore from 'nedb'
import path from 'path'
import md5 from 'md5'

import { Secret } from './config'

let user_db = new Datastore({
    filename: path.join(__dirname, 'data', 'users.json'),
    autoload: true
});

user_db["authenticate"] = function(username, password, callback) {
    user_db.findOne({ userstring: username.toLowerCase() }, (err, user) => {
        if (user == undefined) {
            callback(false)
        } else {
            if (user.password == md5(password + Secret)) {
                callback(true, user)
            } else callback(false)
        }
    });
}

export { user_db as Users }
